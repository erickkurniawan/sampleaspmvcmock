﻿using ASPTDD.Controllers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace ASPTDD.Tests.Controllers
{
    [TestClass]
    public class FizzBuzzGameControllerTest
    {
        [TestMethod]
        public void mengembalikan_angka_1()
        {
            FizzBuzzGameController controller = new FizzBuzzGameController();
            ViewResult result = controller.Index(1) as ViewResult;
            string expected = "1 ";
            string actual = result.ViewBag.Output;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void mengembalikan_12Fizz()
        {
            FizzBuzzGameController controller = new FizzBuzzGameController();
            ViewResult result = controller.Index(3) as ViewResult;
            string expected = "1 2 Fizz ";
            string actual = result.ViewBag.Output;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void mengembalikan_12Fizz4Buzz()
        {
            FizzBuzzGameController controller = new FizzBuzzGameController();
            ViewResult result = controller.Index(5) as ViewResult;
            string expected = "1 2 Fizz 4 Buzz ";
            string actual = result.ViewBag.Output;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void mengembalikan_FizzBuzz()
        {
            FizzBuzzGameController controller = new FizzBuzzGameController();
            ViewResult result = controller.Index(15) as ViewResult;
            string expected = "1 2 Fizz 4 Buzz Fizz 7 8 Fizz Buzz 11 Fizz 13 14 FizzBuzz ";
            string actual = result.ViewBag.Output;
            Assert.AreEqual(expected, actual);
        }
    }
}
