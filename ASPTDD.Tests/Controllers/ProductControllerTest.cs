﻿using ASPTDD.Controllers;
using ASPTDD.Models;
using ASPTDD.Repository;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace ASPTDD.Tests.Controllers
{
    //1.Bisa mengembalikan all product
    //2.Bisa mengambil Product by ID
    //3.Bisa menampilkan Product By Name


    [TestClass]
    public class ProductControllerTest
    {
        public TestContext TestContext { get; set; }
        private List<Product> products;
        private Mock<IProduct> productRepo;
        private IProduct MockProductRepository;
        public ProductControllerTest()
        {
            products = new List<Product>
            {
                 new Product {ID=1,Genre="Novel",Name="Dilan 1990",Price=100},
                 new Product {ID=2,Genre="Komik",Name="Detective Conan",Price=10},
                 new Product {ID=3,Genre="Komik",Name="Conan Barbarian",Price=15},
                 new Product {ID=4,Genre="Komik",Name="Shoot",Price=20}
            };

            productRepo = new Mock<IProduct>();

            productRepo.Setup(p => p.GetAll()).Returns(products);

            productRepo.Setup(p => p.FindById(It.IsAny<int>()))
                .Returns((int i) => products.Where(p => p.ID == i).Single());

            productRepo.Setup(p => p.FindByName(It.IsAny<string>()))
                .Returns((string name) => products.Where(p => p.Name.ToLower().Contains(name)));

            productRepo.Setup(p => p.Insert(It.IsAny<Product>())).
                Returns((Product prod) =>
                {
                    products.Add(prod);
                    return true;
                });

            productRepo.Setup(p => p.Edit(It.IsAny<Product>())).
                Returns((Product prod) =>
                {
                    var original = products.Where(p => p.ID == prod.ID).Single();
                    if (original != null)
                    {
                        original.Name = prod.Name;
                        original.Genre = prod.Genre;
                        original.Price = prod.Price;
                        return true;
                    }
                    else
                        return false;
                });

            MockProductRepository = productRepo.Object;
        }

        [TestMethod]
        public void index_mengembalikan_semua_data_produk()
        {
            ProductController controller = new ProductController(MockProductRepository);
            ViewResult viewResult = controller.Index() as ViewResult;
            var model = viewResult.Model as IEnumerable<Product>;

            Assert.AreEqual(4, model.ToList().Count);
            Assert.AreEqual("Dilan 1990", model.ToList()[0].Name);
        }

        [TestMethod]
        public void pencarian_movie_berdasarkan_id()
        {
            ProductController controller = new ProductController(MockProductRepository);
            ViewResult viewResult = controller.SearchById(1) as ViewResult;
            var model = viewResult.Model as Product;

            Assert.IsNotNull(model);
            Assert.IsInstanceOfType(model, typeof(Product));
            Assert.AreEqual("Dilan 1990", model.Name);
        }

        [TestMethod]
        public void pencarian_movie_berdasarkan_name()
        {
            ProductController controller = new ProductController(MockProductRepository);
            ViewResult viewResult = controller.SearchByName("conan") as ViewResult;
            var model = viewResult.Model as IEnumerable<Product>;

            Assert.IsNotNull(model);
            Assert.IsInstanceOfType(model, typeof(IEnumerable<Product>));
            Assert.AreEqual(2, model.Count());
        }

        [TestMethod]
        public void insert_data_product()
        {
            Product newProduct = new Product
            {
                ID = 5,
                Genre = "Komik",
                Name = "Sword Art Online",
                Price = 12
            };

            ProductController controller = new ProductController(MockProductRepository);
            ViewResult viewResult = controller.InsertPost(newProduct) as ViewResult;
            var model = viewResult.Model as Product;
            var countProduct = MockProductRepository.GetAll().Count();
            bool isInsert = viewResult.ViewBag.isInsert;

            Assert.IsTrue(isInsert);
            Assert.AreEqual(5, countProduct);
            Assert.IsInstanceOfType(model, typeof(Product));
        }

        [TestMethod]
        public void edit_data_product()
        {
            var editProduct = new Product {ID=1, Genre = "Fiksi",
                Name = "Dilan 2018", Price = 15 };

            ProductController controller = new ProductController(MockProductRepository);
            ViewResult viewResult = controller.EditPost(editProduct) as ViewResult;
            var model = viewResult.Model as Product;
            var edit = MockProductRepository.FindById(model.ID);
            bool isEdit = viewResult.ViewBag.isEdit;

            Assert.IsTrue(isEdit);
            Assert.IsNotNull(edit);
            Assert.IsInstanceOfType(edit, typeof(Product));
        }

    }


}
