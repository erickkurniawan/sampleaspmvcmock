﻿using System;
using ASPTDD.Controllers;
using ASPTDD.Repository;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Web.Mvc;
using System.Collections.Generic;
using ASPTDD.Models;
using Unity;
using System.Linq;

namespace ASPTDDSampleIntegrationTests
{
    [TestClass]
    public class ProductTests
    {
        [TestMethod]
        public void cek_get_all_product_method()
        {
            ProductRepository productRepo = new ProductRepository();
            IEnumerable<Product> model = productRepo.GetAll();

            Assert.AreEqual(2, model.ToList().Count);
            Assert.AreEqual("Detective Conan", model.ToList()[0].Name);
        }
    }
}
