﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;

namespace TestUIProject
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            IWebDriver driver = new ChromeDriver(@"C:\library");
            //IWebDriver driver = new FirefoxDriver(@"C:\library");
            //IWebDriver driver = new InternetExplorerDriver(@"C:\library");

            driver.Url = "https://www.google.co.id";
            var searchBox = driver.FindElement(By.Id("lst-ib"));
            searchBox.SendKeys("Epson");
            //searchBox.Submit();
            var btnSubmit = driver.FindElement(By.Name("btnK"));
            btnSubmit.Click();

            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(5);

            var imageLink = driver.FindElements(By.LinkText("Images"))[0];
            imageLink.Click();

            var selectImage = driver.FindElement(By.Name("-tGIyk2QTatzIM:"));
            selectImage.Click();
        }
    }
}
