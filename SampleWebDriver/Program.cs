﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Firefox;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SampleWebDriver
{
    class Program
    {
        static void Main(string[] args)
        {
            IWebDriver driver = new ChromeDriver(@"C:\library");
            //IWebDriver driver = new FirefoxDriver(@"C:\library");
            //IWebDriver driver = new InternetExplorerDriver(@"C:\library");

            driver.Url = "https://www.google.co.id";
            var searchBox = driver.FindElement(By.Id("lst-ib"));
            searchBox.SendKeys("Epson");
            //searchBox.Submit();
            var btnSubmit = driver.FindElement(By.Name("btnK"));
            btnSubmit.Click();

            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(5);

            var imageLink = driver.FindElements(By.LinkText("Images"))[0];
            imageLink.Click();

            var selectImage = driver.FindElement(By.Name("-tGIyk2QTatzIM:"));
            selectImage.Click();
        }
    }
}
