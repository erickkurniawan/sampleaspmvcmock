﻿using System;
using System.Collections.Generic;
using System.Linq;
using ASPTDD.Models;
using ASPTDD.Repository;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ASPTDDIntegration.Test
{
    [TestClass]
    public class ProductIntegrationTest
    {
        [TestMethod]
        public void cek_table_product_return_all_product()
        {
            ProductSQLRepository productRepository = new ProductSQLRepository();
            IEnumerable<Product> model = productRepository.GetAll();

            Assert.AreEqual(2, model.ToList().Count);
            Assert.AreEqual("Detective Conan", model.ToList()[0].Name);
        }
    }
}
