﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ASPTDD.Models
{
    public class Product
    {
        [Key]
        public int ID { get; set; }
        [MaxLength(255)]
        public string Name { get; set; }
        [MaxLength(50)]
        public string Genre { get; set; }
        public decimal Price { get; set; }
    }
}