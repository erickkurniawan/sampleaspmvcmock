﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ASPTDD.Models;
using ASPTDD.Repository;

namespace ASPTDD.Controllers
{
    public class ProductController : Controller
    {
        private IProduct productRepo;

        public ProductController(IProduct productRepo)
        {
            this.productRepo = productRepo;
        }

        public ActionResult Index()
        {
            var model = productRepo.GetAll();
            //return Json(model,JsonRequestBehavior.AllowGet);
            return View(model);
        }

        public ActionResult SearchById(int id)
        {
            var model = productRepo.FindById(id);
            return View(model);
        }

        public ActionResult SearchByName(string name)
        {
            var model = productRepo.FindByName(name);
            return View(model);
        }

        public ActionResult Insert()
        {
            return View();
        }

        [ActionName("Insert")]
        [HttpPost]
        public ActionResult InsertPost(Product newProduct)
        {
            ViewBag.isInsert = productRepo.Insert(newProduct);
            return View(newProduct);
        }

        [ActionName("Edit")]
        [HttpPost]
        public ActionResult EditPost(Product editProduct)
        {
            ViewBag.isEdit = productRepo.Edit(editProduct);
            return View(editProduct);
        }
    }
}