﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ASPTDD.Models;
using System.Data.SqlClient;
using System.Configuration;

namespace ASPTDD.Repository
{
    public class ProductSQLRepository : IProduct
    {
        
        public bool Edit(Product product)
        {
            throw new NotImplementedException();
        }

        public Product FindById(int id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Product> FindByName(string productName)
        {
            throw new NotImplementedException();
        }

        string connStr = 
            ConfigurationManager.ConnectionStrings["MyConnectionString"].ConnectionString;
        public IEnumerable<Product> GetAll()
        {
            using(SqlConnection conn = new SqlConnection(connStr))
            {
                var listProducts = new List<Product>();
                string strSql = @"select * from Products order by Name asc";
                SqlCommand cmd = new SqlCommand(strSql, conn);
                conn.Open();

                SqlDataReader dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        listProducts.Add(new Product
                        {
                            ID = Convert.ToInt32(dr["ID"]),
                            Name = dr["Name"].ToString(),
                            Genre = dr["Genre"].ToString(),
                            Price = Convert.ToDecimal(dr["Price"])
                        });
                    }
                }

                dr.Close();
                cmd.Dispose();
                conn.Close();

                return listProducts;
            }
        }

        public bool Insert(Product product)
        {
            throw new NotImplementedException();
        }
    }
}