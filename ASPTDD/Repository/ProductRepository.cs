﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ASPTDD.Models;

namespace ASPTDD.Repository
{
    public class ProductRepository : IProduct
    {
        private SampleEFDbDataContext db;

        public ProductRepository()
        {
            db = new SampleEFDbDataContext();
        }

        public bool Edit(Product product)
        {
            throw new NotImplementedException();
        }

        public Product FindById(int id)
        {
            //var result = (from p in db.Products
            //              where p.ID == id
            //              select p).SingleOrDefault();

            var result = 
                db.Products.Where(p => p.ID == id).SingleOrDefault();

            //var sqlresult = db.Products.SqlQuery("");

            return result;
        }

        public IEnumerable<Product> FindByName(string productName)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Product> GetAll()
        {
            //var results = db.Products.OrderBy(p => p.Name);
            var results = from p in db.Products
                          orderby p.Name ascending
                          select p;

            return results;
        }

        public bool Insert(Product product)
        {
            throw new NotImplementedException();
        }
    }
}