﻿using ASPTDD.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace ASPTDD.Repository
{
    public class SampleEFDbDataContext : DbContext
    {
        public SampleEFDbDataContext():base("MyConnectionString")
        {
        }
        public DbSet<Product> Products { get; set; }
    }
}