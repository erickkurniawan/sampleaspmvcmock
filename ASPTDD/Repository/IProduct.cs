﻿using ASPTDD.Models;
using System.Collections.Generic;

namespace ASPTDD.Repository
{
    public interface IProduct
    {
        IEnumerable<Product> GetAll();

        Product FindById(int id);

        IEnumerable<Product> FindByName(string productName);

        bool Insert(Product product);

        bool Edit(Product product);
    }
}